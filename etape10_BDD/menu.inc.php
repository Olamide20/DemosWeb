<nav id="menu" class="navbar navbar-expand-sm navbar-light bg-light fixed-top">
    <!-- Le container permet d'aligner les éléments de la barre de navigation sur les autre conteneurs -->
    <div class="container">
        <!-- Icone "home" sur la barre de navigation -->
        <a class="navbar-brand" href="#"><img id="imgEsigelec" src="http://www.esigelec.fr/sites/default/files/esigelec_logo.png" alt="logo Esigelec"></a>
        <!-- Bouton apparaissant pour les petits écrans, à cliquer pour faire apparaitre le menu -->
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#elementsMenu" aria-controls="elementsMenu" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <!-- Elements du menu -->
        <div class="collapse navbar-collapse" id="elementsMenu">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item active">
                    <a class="nav-link" href="#presentation">Présentation</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#enseignements">Enseignements</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#lecole">L'école</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#contactForm">Contact</a>
                </li>
            </ul>
        </div>
    </div>
</nav>
