<?php session_start();

/* Chaque élément du formulaire est récupéré dans le tableau associatif $_POST */

/* si on a essayé d'accéder au script sans passer par le formulaire, on redirige vers une image appropriée et on arrête l'exécution */
if(!isset($_POST['valid_contact'])){
    // http://php.net/manual/fr/function.header.php
    header('Location: http://adhesifservice.fr/295-thickbox_default/panneau-danger-acces-interdit-au-personnel-non-autorise.jpg');
    // https://stackoverflow.com/questions/2747791/why-i-have-to-call-exit-after-redirection-through-headerlocation-in-php
    exit();
}

/* On instancie une variable de session pour la gestion des erreurs. Au départ le formulaire est considéré comme valide */
$_SESSION['form_erreur']=FALSE;

/* On récupère le contenu du champ nom dans la variable $_POST['nom'] (car le champ du formulaire contient l'attribut name="nom")
Le contenu pouvant contenir des éléments dangereux, on valide qu'il ne contient que des caractères alphanumériques, espaces et des tirets. */

function validerNom($nomATester) {
    //Retourne FALSE s'il contient autre chose que des caractères autorisés, ou la chaine.
    return preg_match('/[^a-zA-Z\s]+/', $nomATester) ? FALSE : $nomATester;
}

// récupération du nom depuis le formulaire
$nom_dangereux=$_POST['nom'];

// validation du nom avec la fonction validerNom
$nom_securise=filter_var($nom_dangereux, FILTER_CALLBACK, array('options' => 'validerNom'));

// si $nom_securise contient FALSE, le formulaire n'est pas valide
if ($nom_securise==FALSE){
    // on indique qu'il y a au moins cette erreur pour que l'utilisateur corrige sa saisie
    $_SESSION['form_erreur']=TRUE;
    // on indique le problème pour permettre à l'utilisateur de faire sa correction
    $_SESSION['nom_erreur']="Votre nom ne doit contenir que des lettres, des chiffres, des tirets et des espaces";
    // on met en session pour réafficher, en protégeant contre les attaques XSS
    $_SESSION['nom']=htmlspecialchars($nom_dangereux);
}else{
    // on met en session pour remettre dans le formulaire si un autre champ est invalide
    $_SESSION['nom']=$nom_securise;
}

// récupération du numéro de téléphone
$tel_dangereux=$_POST['telnum'];

// on valide s'il n'est pas vide
if(!empty($tel_dangereux)){
    $tel_securise=filter_var($tel_dangereux, FILTER_VALIDATE_INT);
    if(!$tel_securise){
        // on indique qu'il y a au moins cette erreur pour que l'utilisateur corrige sa saisie
        $_SESSION['form_erreur']=TRUE;
        // on indique le problème pour permettre à l'utilisateur de faire sa correction
        $_SESSION['tel_erreur']="Le numéro de téléphone ne doit contenir que des chiffres";
        // on met en session pour réafficher, en protégeant contre les attaques XSS
        $_SESSION['tel']=htmlspecialchars($tel_dangereux);
    }else{
        if (strlen($tel_securise)!=9){
            // on indique qu'il y a au moins cette erreur pour que l'utilisateur corrige sa saisie
            $_SESSION['form_erreur']=TRUE;
            // on indique le problème pour permettre à l'utilisateur de faire sa correction
            $_SESSION['tel_erreur']='Le numéro saisi doit contenir les 9 chiffres sans le 0';
            // on met en session pour réafficher, en protégeant contre les attaques XSS
            $_SESSION['tel']=htmlspecialchars($tel_dangereux);
        }else{
            // on met en session pour remettre dans le formulaire si un autre champ est invalide
            $_SESSION['tel']=$tel_securise;
        }
    }
}

// récupération de l'email
$courriel_dangereux=$_POST['courriel'];

// on valide s'il n'est pas vide
if(!empty($courriel_dangereux)){
    $courriel_securise=filter_var($courriel_dangereux, FILTER_VALIDATE_EMAIL);
    if(!$courriel_securise){
        // on indique qu'il y a au moins cette erreur pour que l'utilisateur corrige sa saisie
        $_SESSION['form_erreur']=TRUE;
        // on indique le problème pour permettre à l'utilisateur de faire sa correction
        $_SESSION['courriel_erreur']="L'adresse doit respecter la forme xxx@yyy.zzz";
        // on met en session pour réafficher, en protégeant contre les attaques XSS
        $_SESSION['courriel']=filter_var($courriel_dangereux, FILTER_SANITIZE_EMAIL);
    }else{
        $_SESSION['courriel']=$courriel_securise;
    }
}

// récupération du message
$message_dangereux=$_POST['message'];


if($_SESSION['form_erreur']){
    // s'il y a des erreurs ailleurs dans le formulaire il faut le réafficher, en enlevant les éventuels éléments d'attaque XSS
    $_SESSION['message']=htmlspecialchars($message_dangereux);
}else{
    // sinon on élimine les variables de session, car le formulaire a été validé. on ne concerve que le nom qui est affiché dans le message de confirmation.
   unset($_SESSION['tel']);
   unset($_SESSION['courriel']);
   unset($_SESSION['message']);
}


/* on renvoie à la page appelante pour indiquer le succès ou demander des corrections */
header("Location: ".$_SERVER['HTTP_REFERER']."#contactForm");
?>
