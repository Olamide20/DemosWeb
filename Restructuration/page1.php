<?php session_start();
echo 'Au chargement de la page<br><br>';

echo isset($_SERVER['HTTP_REFERER']) ? 'Vous venez de : ' . $_SERVER['HTTP_REFERER'] . '<br>' : 'Pas de REFERER<br>';
echo isset($var_page_1) ? 'var_page_1 vaut ' . $var_page_1 . '<br> ': 'var_page_1 n\'est pas définie<br>';
echo isset($_SESSION['page1']) ? '$_SESSION[\'page1\'] vaut ' . $_SESSION['page1'] . '<br> ': '$_SESSION[\'page1\'] n\'est pas définie<br>';
echo isset($_SESSION['partout']) ? '$_SESSION[\'partout\'] vaut ' . $_SESSION['partout'] . '<br>' : '$_SESSION[\'partout\'] n\'est pas définie <br>';

echo '<br>Affectation de la valeur "PAGE1" à toutes les variables...<br><br>';
$var_page_1="PAGE1";
$_SESSION['page1']="PAGE1";
$_SESSION['partout']="PAGE1";

echo 'Après affectation : <br><br>';

echo isset($var_page_1) ? "var_page_1 vaut " . $var_page_1 . '<br> ': 'var_page_1 n\'est pas définie<br>';
echo isset($_SESSION['page1']) ? '$_SESSION[\'page1\'] vaut ' . $_SESSION['page1'] . '<br> ': '$_SESSION[\'page1\'] n\'est pas définie<br>';
echo isset($_SESSION['partout']) ? '$_SESSION[\'partout\'] vaut ' . $_SESSION['partout'] . '<br>' : '$_SESSION[\'partout\'] n\'est pas définie<br>';

echo '<br><a href="page2.php">Aller à la page 2</a><br>';
echo '<a href="oubli.php?oublier=ok">Oublier toute la session</a>';

?>
